(function(){

    "use strict";

    //SECTIONS
    var top = document.getElementById('js-top');
    var sectionCatch = document.getElementById('js-catch');
    var sectionFeature = document.getElementById('js-feature');
    var sectionService = document.getElementById('js-service');
    var sectionNews = document.getElementById('js-news');

    var navSpotlightTop = document.getElementById('js-spotlight-top');
    var navSpotlightFeature = document.getElementById('js-spotlight-feature');
    var navSpotlightService = document.getElementById('js-spotlight-service');
    var navSpotlightNews = document.getElementById('js-spotlight-news');

    var header = document.getElementById('js-header');

    var controller = new ScrollMagic.Controller();

    //COMPENSATE FOR HERO HEIGHT WHEN RESIZING
    function getHeroHeight() {
        var height = $(window).height();
        return height;
    }

    //INIT CATCH SECTION HEIGHTF
    var CATCH_SECTION_HEIGHT = 710;

    var top = new ScrollMagic.Scene({
      offset: 0,
      duration: CATCH_SECTION_HEIGHT + getHeroHeight(),
      triggerElement: top,
      triggerHook: 'onLeave' //onEnter, onLeave, onCenter, 0.1~1.0
    })
    .setClassToggle(navSpotlightTop, 'is-active')
    .addTo(controller);

    //console.log((CATCH_SECTION_HEIGHT + getHeroHeight()));
    //FEATURE
    new ScrollMagic.Scene({
        offset: -200,
        duration: 640,
        triggerElement: sectionFeature,
        triggerHook: 'onLeave' //onEnter, onLeave, onCenter, 0.1~1.0
    })
    .setClassToggle(navSpotlightFeature, 'is-active')
    .addTo(controller);

    //SERVICE
    new ScrollMagic.Scene({
        offset: -160,
        duration: 660,
        triggerElement: sectionService,
        triggerHook: 'onLeave' //onEnter, onLeave, onCenter, 0.1~1.0
    })
    .setClassToggle(navSpotlightService, 'is-active')
    .addTo(controller);

    //NEWS
    new ScrollMagic.Scene({
        offset: -160,
        duration: 800,
        triggerElement: sectionNews,
        triggerHook: 'onLeave' //onEnter, onLeave, onCenter, 0.1~1.0
    })
    .setClassToggle(navSpotlightNews, 'is-active')
    .addTo(controller);

    $(window).resize(function(){

        //CHECK IF TOP/NEWTOP SCENE EXIST
        if(typeof top !== void || typeof top !== 'undefined'){
            top.destroy(true);
        }else if(typeof newTop !== void || typeof newTop !== 'undefined') {
            newTop.destroy(true);
        }

        //GET NEW HERO HEIGHT
        var heroHeight = getHeroHeight();
        var newDuration = CATCH_SECTION_HEIGHT + heroHeight;

        //CREATE NEW SCENE
        var newTop = new ScrollMagic.Scene({
          offset: 0,
          duration: newDuration,
          triggerElement: top,
          triggerHook: 'onLeave' //onEnter, onLeave, onCenter, 0.1~1.0
        })
        .setClassToggle(navSpotlightTop, 'is-active')
        .addTo(controller);
    });

})();
