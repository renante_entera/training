(function(){
    "use strict";

    //RETURN HEIGHT OF MAIN VISUAL
    function heroHeight() {

        return $('#js-hero').height();
    }

    //WINDOW RESIZE
    function scrollWatch() {
        var h = heroHeight();
        var y = window.pageYOffset;
        var header = $('#js-header');

        if (y > h) {
            header.removeClass('is-static').addClass('is-sticky');

        } else {
            header.removeClass('is-sticky').addClass('is-static');
        }
    }

    $(window).scroll(scrollWatch);
    // END STICKY HEADER
})();
