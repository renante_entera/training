(function(){

    "use strict";

    var featureItems = $('#js-feature-item-one, #js-feature-item-two, #js-feature-item-three');
    var controller = new ScrollMagic.Controller();

    //START FEATURE SECTION PHONE ANIMATION
    new ScrollMagic.Scene({
      triggerElement: featureItems,
      duration: 2250,
      offset: -150
    })
      .on('progress', function (prog) {

        var progress = prog.progress;

        if(progress > 0.6){

            featureItems.removeClass('is-invisible').addClass('is-visible');
        } else {

            featureItems.removeClass('is-visible').addClass('is-invisible');
        }

      })
      .addTo(controller);
      //END FEATURE SECTION PHONE ANIMATION

})();
