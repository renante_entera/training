(function() {
    "use strict";
    var hero = $('#js-hero');
    var heroTextBox = $('#js-hero-text');
    var heroTextHeading = $('#js-hero-text-heading');
    var heroTextSubheading = $('#js-hero-text-subheading');

    //RETURN HEIGHT OF HERO
    function vpHeight() {
        return $(window).height();
    }

    //RETURN WIDTH OF HERO
    function vpWidth() {
        return $(window).width();
    }

    //RETURN HEIGHT OF HERO TEXT
    function heroTextHeight() {

        return heroTextBox.height();
    }

    function centerHeroText() {

        var result = (vpHeight() - (heroTextHeight()+30)) / 2;

        return result;
    }

    function heroAdjust() {
        var height = vpHeight();
        var width = vpWidth();
        var padding = centerHeroText();

        //console.log('height: ' + height + '\nwidth: ' + width + '\npadding: ' + padding);

        hero.css({
            'min-height' : height
        });

        heroTextBox.css({
            'max-height' : height,
            'padding-top' : padding
        });

        //
        if(width > 1200){

            heroTextHeading.css({
                'font-size' : '160px'
            });
            heroTextSubheading.css({
                'font-size' : '60px'
            });
            //console.log('font-size: 160px: width > 1200');
        }else if (width > 1000 && width < 1200) {

            heroTextHeading.css({
                'font-size' : '130px'
            });
            heroTextSubheading.css({
                'font-size' : '50px'
            });
            //console.log('font-size: 130px: width > 800 && width < 1000');
        }else if (width > 800 && width < 1000) {

            heroTextHeading.css({
                'font-size' : '100px'
            });
            heroTextSubheading.css({
                'font-size' : '40px'
            });
            //console.log('font-size: 100px: width > 1000 && width < 1000');
        }else if(width > 600 && width < 800) {
            heroTextHeading.css({
                'font-size' : '70px'
            });
            heroTextSubheading.css({
                'font-size' : '27px'
            });
            //console.log('font-size: 70px: width > 600 && width < 800');
        }else {
            heroTextHeading.css({
                'font-size' : '50px'
            });
            heroTextSubheading.css({
                'font-size' : '20px'
            });
        }
    }
    //ON IMPACT
    heroAdjust();
    hero.css({
        'min-height' : vpHeight()
    });
    heroTextBox.css({
        'padding-top' : centerHeroText()
    });


    //console.log('Hero: ' + vpHeight() + ' Text: ' + heroTextHeight() + ' Result: ' + centerHeroText());

    //ON RESIZE
    $(window).resize(function(){
        heroAdjust();
    });

})();
