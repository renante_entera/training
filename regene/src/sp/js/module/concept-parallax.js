(function(){

    "use strict";

    var trigger = $('#js-hero');
    var phone1 = $('#js-concept-prlx-phone1');
    var phone2 = $('#js-concept-prlx-phone2');
    var phone3 = $('#js-concept-prlx-phone3');

    var controller = new ScrollMagic.Controller();

    //START concept SECTION PHONE ANIMATION
    new ScrollMagic.Scene({
      triggerElement: phone3,
      duration: 3000,
      offset: $(window).height()

      //offset: 100 ORIG
    })
      .on('progress', function (prog) {

        var progress = prog.progress;

        phone1.css({'transform' : 'translate3d(0,' + (-progress * 725) + 'px, 0)'});
        //phone2.css({'transform' : 'translate3d(0,' + (progress * 325) + 'px, 0)'});
        phone3.css({'transform' : 'translate3d(0,' + (progress * 1125) + 'px, 0)'});
      })
      .addTo(controller);
      //END concept SECTION PHONE ANIMATION

})();
