(function() {
    "use strict";
    var hero = $('#js-hero');
    var navListTextBox = $('#js-nav-list');
    var heroTextBox = $('#js-hero-inner');
    var heroTextHeading = $('#js-hero-text-heading');
    var heroTextSubheading = $('#js-hero-text-subheading');

    //RETURN HEIGHT OF HERO
    function vpHeight() {
        return $(window).innerHeight();
    }

    //RETURN WIDTH OF HERO
    function vpWidth() {
        return $(window).innerWidth();
    }

    //RETURN HEIGHT OF HERO TEXT
    function heroTextHeight() {

        return heroTextBox.height();
    }

    function centerHeroText() {

        var result = (vpHeight() - (heroTextHeight()+30)) / 2;

        return result;
    }

    function heroAdjust() {
        var height = vpHeight();
        var width = vpWidth();
        var padding = centerHeroText();

        //console.log('height: ' + height + '\nwidth: ' + width + '\npadding: ' + padding);

        hero.css({
            'min-height' : height,
            'min-width' : width
        });

        heroTextBox.css({
            'max-height' : height,
            'padding-top' : padding
        });

        heroTextHeading.css({
            'font-size' : '40px'
        });
        heroTextSubheading.css({
            'font-size' : '15px'
        });
    }
    //ON IMPACT
    heroAdjust();

    hero.css({
        'min-height' : vpHeight()
    });
    heroTextBox.css({
        'padding-top' : centerHeroText()
    });

    //ON RESIZE
    $(window).resize(function(){
        heroAdjust();
    });

})();
