(function() {
    "use strict";
    var HEADER_OFFSET = 97;
    $(window).ready(function() {
        $('a[href*=\\#]:not([href=\\#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {

                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');

                if (target.length) {
                    $('html,body').animate({
                        scrollTop: (target.offset().top - HEADER_OFFSET)
                    }, 800);
                    return false;
                }
            }
        });
    });

})();
