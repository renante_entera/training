(function() {
    "use strict";

    $('.feature').slick({
        autoplay: false,
        arrows: true,
        prevArrow: '<button id="js-button-prev" type="button" class="slick-prev arrow arrow--slick-prev"><div class="arrow__ring arrow__ring--slick"><span class="pointer pointer--slick pointer--left"></span></div></button>',
        nextArrow: '<button id="js-button-next" type="button" class="slick-next arrow arrow--slick-next"><div class="arrow__ring arrow__ring--slick"><span class="pointer pointer--slick pointer--right"></span></div></button>',
        infinite: false,
        slidesToSroll: 3,
        swipe: true
    });


    //console.log( $('.slick-prev').hasClass('slick-disabled'));
    //console.log( next.hasClass('slick-disabled'));

    //ON IMPACT
    $('.slick-prev').attr('disabled', 'disabled').addClass('is-disabled');

    function prevState(){
        if($('#js-button-prev').attr('disabled') === 'disabled') return true;
        return  false;
    }

    function nextState(){
        if($('#js-button-next').attr('disabled') === 'disabled') return true;
        return  false;
    }

    function check(prev, next) {

        //IF PREV IS DISABLED
        if($('#js-button-prev').attr('disabled') === 'disabled'){
            $('#js-button-prev').removeAttr('disabled').removeClass('is-disabled');
        }

    }

    //console.log($('.slick-prev').hasClass('slick-disabled') + ' ' + $('.slick-next').hasClass('slick-disabled'));

    $('.slick-prev, .slick-next').each(function(){
        $(this).click(function(){
            //console.log($('.slick-prev').hasClass('slick-disabled') + ' ' + $('.slick-next').hasClass('slick-disabled'));

            //FALSE && FALSE
            if($('.slick-prev').hasClass('slick-disabled') === false && $('.slick-next').hasClass('slick-disabled') === false){

                if(prevState()) $('#js-button-prev').removeAttr('disabled').removeClass('is-disabled');

                if(nextState()) $('#js-button-next').removeAttr('disabled').removeClass('is-disabled');

            }else if($('.slick-prev').hasClass('slick-disabled') === false && $('.slick-next').hasClass('slick-disabled') === true){

                if(!nextState()) $('#js-button-next').attr('disabled', 'disabled').addClass('is-disabled');

            }else if($('.slick-prev').hasClass('slick-disabled') === true && $('.slick-next').hasClass('slick-disabled') === false){

                if(!prevState()) $('#js-button-prev').attr('disabled', 'disabled').addClass('is-disabled');
            }

        });
    });

    $('.feature').on('swipe', function(e){
        //FALSE && FALSE
        if($('.slick-prev').hasClass('slick-disabled') === false && $('.slick-next').hasClass('slick-disabled') === false){

            if(prevState()) $('#js-button-prev').removeAttr('disabled').removeClass('is-disabled');

            if(nextState()) $('#js-button-next').removeAttr('disabled').removeClass('is-disabled');

        }else if($('.slick-prev').hasClass('slick-disabled') === false && $('.slick-next').hasClass('slick-disabled') === true){

            if(!nextState()) $('#js-button-next').attr('disabled', 'disabled').addClass('is-disabled');

        }else if($('.slick-prev').hasClass('slick-disabled') === true && $('.slick-next').hasClass('slick-disabled') === false){

            if(!prevState()) $('#js-button-prev').attr('disabled', 'disabled').addClass('is-disabled');
        }
    });

})();
