(function(){
    "use strict";

    //RETURN HEIGHT OF MAIN VISUAL
    function heroHeight() {

        return $('#js-hero').height();
    }

    //RETURN VIEWPORT WIDTH
    function vpWidth() {

        return $(window).width();
    }

    function headerWidth() {
        var header = document.getElementById('js-header');
        //var hWidth = document.getElementById('js-')
        //header.getBoundingClientRect();
        console.log(header.getBoundingClientRect()['width']);
        //return header;
    }
    headerWidth();
    function scrollWatch() {
        var h = heroHeight();
        var y = window.pageYOffset;
        var header = $('#js-header');

        if (y > h) {
            header.removeClass('is-static').addClass('is-sticky');

        } else {
            header.removeClass('is-sticky').addClass('is-static');
        }
    }

    //ON IMPACT
    var width = vpWidth();
    //console.log(width);

    if(width > 640){
        $(window).scroll(scrollWatch);
        console.log('more than');
    }else {
        var header = $('#js-header');

        if(header.hasClass('is-static')){
            header.removeClass('is-static');
        }
    }


    //WINDOW RESIZE
    $(window).resize(function(){
        var width = vpWidth();

        if(width > 640){
            $(window).scroll(scrollWatch);
        }
    });

    // END STICKY HEADER
})();
