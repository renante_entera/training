(function(){
    "use strict";

    var transformicon = $('#js-transformicon');
    var header = $('#js-header');
    var nav = $('#js-nav');
    var navListTextBox = $('#js-nav-list');
    var scroll = true;

    //RETURN HEIGHT OF HERO
    function vpHeight() {
        return $(window).innerHeight();
    }

    //RETURN HEIGHT OF NAV LIST
    function navListHeight() {

        return navListTextBox.height();
    }

    //VERTICALLY CENTER NAV LIST
    function navMarginTop() {
        var height = vpHeight();
        var LIST_HEIGHT = 216;
        var OFFSET_MARGIN = 30;
        var result = ((height - (LIST_HEIGHT + OFFSET_MARGIN)) / 2);

        return result;
    }

    function getScrollState() {
        return scroll;
    }

    function setScrollState(state) {
        scroll = state;
    }

    function toggleScrollState() {

        if(getScrollState()){
            scrollDisabled();
            setScrollState(false);
        }else {
            scrollEnabled();
            setScrollState(true);
        }
    }

    function scrollDisabled() {
        $('body').bind('touchmove', function(e){e.preventDefault()});
    }

    function scrollEnabled() {
        $('body').unbind('touchmove');
    }

    transformicon.click(function(){
        $(header).toggleClass('is-expanded');
        $(this).toggleClass('is-active');
        $(nav).toggleClass('is-visible');

        nav.css({
            'margin-top' : navMarginTop()
        });
        toggleScrollState();
        //$('#js-body').toggleClass('u-hide-overflow');
    });

})();
