/**
 * @constructor
 */
 
(() => {
    $(function() {
        $(window).scroll(function() {
            const winTop = $(window).scrollTop();
            if (winTop >= 105) {
                $("body").addClass("is-sticky-header");
            } else {
                $("body").removeClass("is-sticky-header");
            } //if-else
        }); //win func.
    }); //ready func.
    
    //share button
    $(function() {
        $('.share').click(function() {
            window.open(encodeURI(decodeURI(this.href)), 'sharewindow', 'width=550, height=450, personalbar=0, toolbar=0, scrollbars=1, resizable=!');
            return false;
        });
    });
    
    //share button single.html
    $(function() {
        $('.before-share-list,.after-share-list-other').click(function() {
            $('.more-list', this).slideToggle('fast');
        });
    });
    
   
})();