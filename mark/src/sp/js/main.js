'use strict';


//Sllick Carousel
$('.feature__image').slick({
    infinite: false,
    dots: false,
    prevArrow: $('.feature__button__left'),
    nextArrow: $('.feature__button__right')
});

const $navbar = $('#js-menu');
const $overlay = $('.nav__overlay');
const $body = $(document.body);
const $navmenu = $('.nav__logo__text, .nav__right__menu__top, .nav__right__menu__middle, .nav__right__menu__bottom');
const ACTIVE = '--active';
const OPEN = '--open';


//Navbar
$overlay.hide();
$navbar.click(function(){
    $(this).toggleClass(OPEN);
    $overlay.fadeToggle(1000);

    if($(this).hasClass(OPEN)){
        $navmenu.addClass(ACTIVE);
        $body.css({"overflow": "hidden" });
    } else {
        $navmenu.removeClass(ACTIVE);
        $body.css({"overflow": "visible" });
    }

});

$('.nav__link').click(function(){
    setTimeout(function(){ $overlay.fadeOut(1000);  }, 100);
    $navbar.removeClass(OPEN);

    if($(this).hasClass(OPEN)){
        $body.css({"overflow": "hidden" });
    } else {
        $body.css({"overflow": "visible" });
    }

    if($navmenu.hasClass(ACTIVE)) {
        $navmenu.removeClass(ACTIVE);
    }
});

$("html, body").on("scroll touchmove", function(e){
    if ($navbar.hasClass(OPEN)) {
        e.preventDefault();
    }
});



import stickyHeader from './module/_stickyHeader.js';
stickyHeader();

$(window).on('scroll resize', function () {
    stickyHeader();
});
