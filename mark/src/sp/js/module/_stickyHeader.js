'use strict';

//Navbar Section

export default function stickyHeader() {

    const $nav = $('#js-navbar');
    const STICKY_HEADER = '--is-sticky';
    const $window = $(window);
    let scrollPos = $window.scrollTop();


    if( scrollPos >= 30 ) {
        $nav.addClass(STICKY_HEADER);
    } else {
        $nav.removeClass(STICKY_HEADER);
    }


}
