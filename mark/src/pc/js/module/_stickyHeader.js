'use strict';

//Navbar Section

export default function stickyHeader() {

    const $navbar = $('#js-navbar');
    const STICKY_HEADER = 'is-sticky';
    const $window = $(window);
    let windowHeight = $window.height();
    let scrollPos = $window.scrollTop();

    if( windowHeight <= scrollPos ) {
        $navbar.addClass(STICKY_HEADER);
    } else {
        $navbar.removeClass(STICKY_HEADER);
    }


}
