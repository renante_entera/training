'use strict';

import stickyHeader from './module/_stickyHeader.js';
stickyHeader();

$(window).on('scroll resize orientationchange', function () {
    stickyHeader();
});


var controller = new ScrollMagic.Controller({
});

// Copy Section
const $sectionOne =  $('#js-sec0');
const $sectionTwo = $('#js-sec1');
const $sectionOneHeight = $sectionOne.height();


new ScrollMagic.Scene({
    triggerElement: $sectionOne[0],
    triggerHook: 'onEnter',
    duration: $sectionOneHeight
})

    .on('progress', function(event){
        for (var iCellphone = 1; iCellphone <= 3; iCellphone++) {
            const $cellphone = $('#js-cellphone'+ [iCellphone]);
            $cellphone.css({
                'transform': 'translateY(' + ( - event.progress * 150) + 'px)'
            });
        }
    })

    .addTo(controller);

//Feature Section
new ScrollMagic.Scene({
    triggerElement: $sectionTwo[0],
    triggerHook: 'onEnter',
    offset: 150
})
        .setClassToggle ('.js-feature', 'on-enter')
        .addTo(controller);

//Navbar Section
for(var i=0; i <= 3; i+=1) {
    new ScrollMagic.Scene({
        triggerElement: '#js-sec' + i,
        triggerHook: 0.25,
        duration: document.getElementById('js-sec' + i).offsetHeight
    })
    .setClassToggle('#js-nav' + i, 'is-active')
    .addTo(controller);
}
