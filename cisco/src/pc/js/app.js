import Stickyheader from './module/_stickyHeader';
import Moveup from './module/_moveUp';
import Scrollfade from './module/_scrollFade';
import SmoothScroll from './module/_smoothScroll';
import Menucurrent from './module/_menuCurrent';

window.addEventListener('load', () => {
   new Stickyheader();
   new Moveup();
   new Scrollfade();
   new SmoothScroll();
   new Menucurrent();
});