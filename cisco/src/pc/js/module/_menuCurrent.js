import ScrollMagic from 'scrollmagic';


export default class Menucurrent {

    constructor() {
        this.controller = new ScrollMagic.Controller({
            globalSceneOptions: {
                reverse: true
            }
        });
        this.target = ['top', 'feature', 'service', 'news'];
        this.sectionLength = document.getElementsByClassName('js-current').length;

        this.bind();
    }


    bind() {
        for(let i = 0; i < this.sectionLength; i++) {
            new ScrollMagic.Scene({
                triggerElement: '#' + this.target[i],
                triggerHook: 0.15,
                duration: document.getElementById(this.target[i]).offsetHeight
            })
                .setClassToggle('#js-menu-' + this.target[i], 'is-current')
                .addTo(this.controller);
        }
    }
}