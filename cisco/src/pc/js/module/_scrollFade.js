import ScrollMagic from 'scrollmagic';

export default class Scrollfade {

    constructor() {
        this.controller = new ScrollMagic.Controller();
        this.feature = document.getElementById('js-feature');

        this.bind();
    }

    bind() {
        new ScrollMagic.Scene({
            triggerElement: this.feature,
            triggerHook: 0.3 //onEnter, onLeave, onCenter, 0.1~1.0
        })
            .setClassToggle('.feature-content-item', 'is-active')
            .addTo(this.controller);
    }
}
