export default class Smoothscroll {

    constructor() {
        this.headerMenu = document.querySelector('.header-menu');

        this.bind();
    }


    bind() {
        this.headerMenu.addEventListener('click', (ev) => {
            this.handleClick(ev);
        });
    }

    handleClick(ev) {
        let target = ev.target;
        let id = target.getAttribute('href');
        let scrollPosition = document.querySelector(id).offsetTop;

        if (id !== null && id.length > 0) {
            ev.preventDefault();

            TweenMax.to(window, 0.5, {
                scrollTo: {
                    y: scrollPosition - 40,
                    autoKill: true
                },
                ease: Cubic.easeInOut
            });
        }
    }
}