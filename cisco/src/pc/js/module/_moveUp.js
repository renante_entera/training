import ScrollMagic from 'scrollmagic';

export default class Moveup {

    constructor() {
        this.controller = new ScrollMagic.Controller();
        this.catch = document.getElementById('js-catch');
        this.spModel = document.getElementsByClassName('catch-image-item');

        this.bind();
    }

    bind() {
        this.hook = [.2, .03, .13];
        this.duration = [500, 1000, 1000];
        this.moveSpeed = [250, 800, 600];

        for(let i = 0; i < this.spModel.length; i++) {
            this.spMove(i);
        }
    }

    // spMove
    spMove(num) {
        new ScrollMagic.Scene({
            triggerElement: this.catch,
            triggerHook: this.hook[num],
            duration: this.duration[num]
        })
            .on('progress', (prog) => {
                let progress = prog.progress;
                this.spModel[num].style.transform = 'translate3d(0,' + -(progress * this.moveSpeed[num]) + 'px,0)';
            })
            .addTo(this.controller);
    }
}