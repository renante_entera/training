import ScrollMagic from 'scrollmagic';

export default class Stickyheader {

    constructor() {
        this.controller = new ScrollMagic.Controller();
        this.headerMenu = document.getElementById('js-sticky');
        
        this.bind();
    }

    bind() {
        this.fixedPoint();

        window.addEventListener('resize', () => {
            if(window.innerWidth > 982) {
                this.scene.triggerElement().style.width = window.innerWidth + 'px';
            }
        });
    }
    

    // hedeer fixed
    fixedPoint() {
        this.scene = new ScrollMagic.Scene({
            triggerElement: this.headerMenu,
            triggerHook: 0 //onEnter, onLeave, onCenter, 0.1~1.0
        })
            .setPin(this.headerMenu)
            .addTo(this.controller);
    }
}
