#Overview

This is a sample web site for your training.
This page needs coding by HTML5, CSS3, JavaScript.
Adapt Retina display.

Please try in the folowing order.

1. Writing HTML & CSS
2. After finishing HTML & CSS, writing JavaScript

#Git repository

https://bitbucket.org/lig-admin/training

Please use this repository.
Let's start after creating your directory.

```
project/
 |- kota/
 |- regene/
 |- patrick/
 |- haru/
 |- sisuko/
```

Please use git-flow, and send Seito pull-requests one by one.
Seito look over your source code, and send you feedbacks.

#Tools & Techniques

You shoud use them.

+ sprite
+ changing icons webfont by iconmoon
+ grid system
+ SMACSS
+ frontplate
+ style guide (frontnote)
+ git-flow

And this plugins will help you.

+ http://scrollmagic.io/

#Specification

##font
Please use them

+ Helvetica
+ Lato (https://www.google.com/fonts/specimen/Lato)


##hero

+ Fit this section to the monitor size

*Sample*
http://liginc.co.jp/edimo/

##header

+ Sticky slider
+ Auto current style of navigations with scrolling
+ Navigations has smooth scroll animations

*Sample*
http://www.shiftbrain.co.jp/book/jquery/sample/chapter05/03/

##Catch copy text catch copy !! Catch copy text!

+ Pallarax

*Sample*
http://www.ok-studios.de/home/
(Please look the section "PLATZ HIRSCHE")

##Feature

+ fadeIn animation

##Service

None

##News

None

##Banners

None

##Mail form

+ Add hover animation to button

##footer

+ Add hover animation to SNS link