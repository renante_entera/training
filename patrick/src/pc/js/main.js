//start ------------------------------------------------------------------------------------------------------------------------
$().ready(function(){
	//sticky header
	var ID_HEADER="#js-header";
	var CLASS_NAME_STICKY_HEADER="is-sticky";
	//
	new StickyHeader($(ID_HEADER), CLASS_NAME_STICKY_HEADER);


	//scale element to window size
	var ID_MAIN_IMAGE_CONTAINER="#js-main-container";
	var ID_MAIN_IMAGE="#js-main-image";
	var	MAIN_IMAGE_WIDTH=1080;
	var MAIN_IMAGE_HEIGHT=617;
	//
	new ScalelementToWindowSize($(ID_MAIN_IMAGE), $(ID_MAIN_IMAGE_CONTAINER), MAIN_IMAGE_WIDTH, MAIN_IMAGE_HEIGHT);


	//scrollMagic controller
	var controller=new ScrollMagic.Controller();

	//
	sectionCatchAnimation(controller);

	//
	sectionFeatureAnimation(controller);

	//scroll spy
	var numPrevHeight=$(window).height();
	var isHeightChanged=false;
	var arrScrollSpyScenes=null;
	$(window).resize(function(){
		if( numPrevHeight != $(window).height() ){
			numPrevHeight=$(window).height();
			isHeightChanged=true;
		}
	});
	$(window).scroll(function(){
		if(isHeightChanged == false) return;
		//destroy prev scenes
		for(let i=0; i < arrScrollSpyScenes.length; i++){
			arrScrollSpyScenes[i].remove();
			arrScrollSpyScenes[i].destroy(true);
		}
		//
		isHeightChanged=false;
		arrScrollSpyScenes=navScrollSpyAnimation(controller);
	});
	arrScrollSpyScenes=navScrollSpyAnimation(controller);

});




//functions ------------------------------------------------------------------------------------------------------------------------
function navScrollSpyAnimation(scrollMagicController){
	var ID_HEADER="#js-header";
	var ID_SECTION_TOP="#js-section-top";
	var ID_SECTION_FEATURE="#js-section-feature";
	var ID_SECTION_SERVICE="#js-section-service";
	var ID_SECTION_NEWS="#js-section-news";
	var ID_NAV_ITEM_TOP="#js-nav-item-top";
	var ID_NAV_ITEM_FEATURE="#js-nav-item-feature";
	var ID_NAV_ITEM_SERVICE="#js-nav-item-service";
	var ID_NAV_ITEM_NEWS="#js-nav-item-news";
	var CLASS_NAME_IS_SELECTED="is-selected";
	//
	var numHeaderHeight=$(ID_HEADER)[0].getBoundingClientRect().height;
	var numAdditionalOffset=70 * 2; //based from section bottom padding
	var numViewPort=$(window).height() - numHeaderHeight - numAdditionalOffset;
	//
	var scene1=new ScrollMagic.Scene({
		triggerElement:$(ID_SECTION_TOP)[0],
		triggerHook:"onEnter",
		duration:$(ID_SECTION_TOP)[0].getBoundingClientRect().height,
		offset:numViewPort
	})
		.setClassToggle($(ID_NAV_ITEM_TOP)[0], CLASS_NAME_IS_SELECTED)
		.addTo(scrollMagicController);
	//
	var scene2=new ScrollMagic.Scene({
		triggerElement:$(ID_SECTION_FEATURE)[0],
		triggerHook:"onEnter",
		duration:$(ID_SECTION_FEATURE)[0].getBoundingClientRect().height,
		offset:numViewPort
	})
		.setClassToggle($(ID_NAV_ITEM_FEATURE)[0], CLASS_NAME_IS_SELECTED)
		.addTo(scrollMagicController);
	//
	var scene3=new ScrollMagic.Scene({
		triggerElement:$(ID_SECTION_SERVICE)[0],
		triggerHook:"onEnter",
		duration:$(ID_SECTION_SERVICE)[0].getBoundingClientRect().height,
		offset:numViewPort
	})
		.setClassToggle($(ID_NAV_ITEM_SERVICE)[0], CLASS_NAME_IS_SELECTED)
		.addTo(scrollMagicController);
	//
	var scene4=new ScrollMagic.Scene({
		triggerElement:$(ID_SECTION_NEWS)[0],
		triggerHook:"onEnter",
		duration:$(ID_SECTION_NEWS)[0].getBoundingClientRect().height,
		offset:numViewPort
	})
		.setClassToggle($(ID_NAV_ITEM_NEWS)[0], CLASS_NAME_IS_SELECTED)
		.addTo(scrollMagicController);
	//
	return [scene1, scene2, scene3, scene4];
}

function sectionCatchAnimation(scrollMagicController){
	//
	var ID_SECTION_CATCH="#js-section-catch";
	var ID_SECTION_CATCH_TITLE="#js-section-catch-title";
	var ID_SECTION_CATCH_DESCRIPTION="#js-section-catch-description";
	var ID_SECTION_CATCH_PHONE_1="#js-section-catch-phone-1";
	var ID_SECTION_CATCH_PHONE_2="#js-section-catch-phone-2";
	var ID_SECTION_CATCH_PHONE_3="#js-section-catch-phone-3";

	//
	var sceneCatch=new ScrollMagic.Scene({
		triggerElement:$(ID_SECTION_CATCH)[0],
		triggerHook:"onEnter",
		duration:$(window).height() + $(ID_SECTION_CATCH)[0].getBoundingClientRect().height,
		//duration:$(window).height(),
		offset:0
	});

	//
	scrollMagicController.addScene(sceneCatch);


	//simple effect class definition
	var ParallaxEffect=function ParallaxMovement(htmlElement, arrObjParams){
		this.htmlElement=htmlElement;
		this.arrObjParams=arrObjParams;
		//
		var objParam;
		for(var i=0; i < arrObjParams.length; i++){
			objParam=arrObjParams[i];
			objParam.result=0;
		}
	}
	ParallaxEffect.prototype.update=function(numProgress){
		var objParam;
		for(var i=0; i < this.arrObjParams.length; i++){
			objParam=this.arrObjParams[i];
			objParam.result=objParam.numStart + (objParam.numEnd - objParam.numStart) * (numProgress + objParam.numProgressOffset);
			$(this.htmlElement).css(objParam.strProp, objParam.result);	
		}
	}
	//use effect class
	var parallaxEffectCatchPhone1=new ParallaxEffect(
		$(ID_SECTION_CATCH_PHONE_1),
		[{
			strProp:"top",
			numStart:218 + 400,
			numEnd:218,
			numProgressOffset:0.5}]
	);
	var parallaxEffectCatchPhone2=new ParallaxEffect(
		$(ID_SECTION_CATCH_PHONE_2),
		[{
			strProp:"top",
			numStart:314 + 200,
			numEnd:314,
			numProgressOffset:0.5}]
	);
	var parallaxEffectCatchPhone3=new ParallaxEffect(
		$(ID_SECTION_CATCH_PHONE_3),
		[{
			strProp:"top",
			numStart:100 + 100,
			numEnd:100,
			numProgressOffset:0.5}]
	);
	var parallaxEffectCatchTitle=new ParallaxEffect(
		$(ID_SECTION_CATCH_TITLE),
		[{
			strProp:"top",
			numStart:225 + 100,
			numEnd:225,
			numProgressOffset:0.5}]
	);
	var parallaxEffectCatchDescription=new ParallaxEffect(
		$(ID_SECTION_CATCH_DESCRIPTION),
		[{
			strProp:"top",
			numStart:400 + 100,
			numEnd:400,
			numProgressOffset:0.5}]
	);
	//update on event
	sceneCatch.on("progress", function(e){
		//console.log(e.progress);
		parallaxEffectCatchPhone1.update(e.progress);
		parallaxEffectCatchPhone2.update(e.progress);
		parallaxEffectCatchPhone3.update(e.progress);
		parallaxEffectCatchTitle.update(e.progress);
		parallaxEffectCatchDescription.update(e.progress);
	});
}

function sectionFeatureAnimation(scrollMagicController){
	//
	var ID_PREFIX="#js-feature-";
	var CLASS_NAME_TOGGLE="is-shown";
	var arrStrArticles=["-html5", "-css3", "-javascript"];
	//
	for(let i=0; i < arrStrArticles.length; i++){

		let strArticle=arrStrArticles[i];

		new ScrollMagic.Scene({
			triggerElement:$(ID_PREFIX + "image" + strArticle)[0],
			triggerHook:"onEnter",
		})	
			.setClassToggle( $(ID_PREFIX + "image" + strArticle)[0], CLASS_NAME_TOGGLE )
			.addTo(scrollMagicController);

		new ScrollMagic.Scene({
			triggerElement:$(ID_PREFIX + "title" + strArticle)[0],
			triggerHook:1,
		})	
			.setClassToggle( $(ID_PREFIX + "title" + strArticle)[0], CLASS_NAME_TOGGLE )
			.addTo(scrollMagicController);

		new ScrollMagic.Scene({
			triggerElement:$(ID_PREFIX + "description" + strArticle)[0],
			triggerHook:1,
		})	
			.setClassToggle( $(ID_PREFIX + "description" + strArticle)[0], CLASS_NAME_TOGGLE )
			.addTo(scrollMagicController);
		//console.log($(ID_PREFIX + "image" + strArticle)[0]);
	}
}



//classes ------------------------------------------------------------------------------------------------------------------------
var ScalelementToWindowSize=(function(){
	function ScaleElementToWindowSize(htmlElementTarget, htmlElementTargetContainer, numMinWidth, numMinHeight){
		this.htmlElementTarget=htmlElementTarget;
		this.htmlElementTargetContainer=htmlElementTargetContainer;
		this.numMinWidth=numMinWidth;
		this.numMinHeight=numMinHeight;

		//
		this.initEvents();

		//
		this.update();
	}

	ScaleElementToWindowSize.prototype.initEvents=function(){
		var _this=this;
		$(window).resize(function(){
			_this.update();
		});
	}

	ScaleElementToWindowSize.prototype.update=function(){
		var numScale=1;

		//initially fit 100% to the lowest side
		if( $(window).height() < $(window).width() ){
			//console.log("h");
			numScale=$(window).height() / this.numMinHeight;
		}else{
			//console.log("w");
			numScale=$(window).width() / this.numMinWidth;
		}
		//numScale=numScale - 1;
		//console.log($(window).height(), numMinHeight, numScale);
		$(this.htmlElementTarget).css("min-width", this.numMinWidth);
		$(this.htmlElementTarget).css("min-height", this.numMinHeight);
		$(this.htmlElementTarget).css("transform-origin", "left top");
		$(this.htmlElementTarget).css("transform", "scale("+ numScale +")");
		
		//
		var widthDifference=$(window).width() - this.htmlElementTarget[0].getBoundingClientRect().width;
		var heightDifference=$(window).height() - this.htmlElementTarget[0].getBoundingClientRect().height;

		//
		//console.log("---");
		//console.log( $(window).width(), $(window).height() );
		//console.log( widthDifference, heightDifference );
		//console.log( numScale );

		
		//if there is extra in differences, refit the side with difference
		if(widthDifference > 0){
			numScale=$(window).width() / this.numMinWidth;
		}
		if(heightDifference > 0){
			numScale=$(window).height() / this.numMinHeight;
		}
		$(this.htmlElementTarget).css("transform-origin", "left top");
		$(this.htmlElementTarget).css("transform", "scale("+ numScale +")");

		//update the container box dimensions
		$(this.htmlElementTargetContainer).css("width", this.htmlElementTarget[0].getBoundingClientRect().width);
		$(this.htmlElementTargetContainer).css("height", this.htmlElementTarget[0].getBoundingClientRect().height);

		//console.log(this.htmlElementTarget[0].getBoundingClientRect().width, this.htmlElementTarget[0].getBoundingClientRect().height);
	}

	return ScaleElementToWindowSize;
}());

var StickyHeader=(function(){
	function StickyHeader(htmlElementTarget, strClassNameSticky, strCloneSuffix="-clone"){
		//
		this.htmlElementTarget=htmlElementTarget;
		this.strClassNameSticky=strClassNameSticky;
		this.strCloneSuffix=strCloneSuffix;

		//
		this.initEvents();

		//
		this.update();
	}

	StickyHeader.prototype.initEvents=function(){
		//
		var _this=this;

		//
		$(window).scroll(function(){
			_this.update();
		});
	}

	StickyHeader.prototype.update=function(){
		//
		var strCloneID="#" + $(this.htmlElementTarget).attr("id") + this.strCloneSuffix;

		//
		var htmlElementTargetClone=null;
		if( $(strCloneID).length == 0){
			htmlElementTargetClone=$("<div>&nbsp;</div>");
			htmlElementTargetClone.attr("id", $(this.htmlElementTarget).attr("id") + this.strCloneSuffix);
			htmlElementTargetClone.attr("class", $(this.htmlElementTarget).attr("class") );
			htmlElementTargetClone.css("position", "absolute");
			//htmlElementTargetClone.css("background-color", "#ff0000");

			//
			$(this.htmlElementTarget).before( htmlElementTargetClone );
		}else{
			htmlElementTargetClone=$(strCloneID);
		}

		//
		if( $(htmlElementTargetClone)[0].getBoundingClientRect().top <= 0 ){
			htmlElementTargetClone.css("position", "static");
			$(this.htmlElementTarget).addClass(this.strClassNameSticky);
		}else{
			htmlElementTargetClone.css("position", "absolute");
			$(this.htmlElementTarget).removeClass(this.strClassNameSticky);
		}
	}

	return StickyHeader;
}());
