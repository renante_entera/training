//start ------------------------------------------------------------------------------------------------------------------------
$().ready(function(){	
	//sticky header
	//var ID_HEADER="#js-header";
	//var CLASS_NAME_STICKY_HEADER="is-sticky";
	//
	//new StickyHeader($(ID_HEADER), CLASS_NAME_STICKY_HEADER);




	//scale element to window size
	var ID_MAIN_IMAGE_CONTAINER="#js-main-container";
	var ID_MAIN_IMAGE="#js-main-image";
	var	MAIN_IMAGE_WIDTH=320;
	var MAIN_IMAGE_HEIGHT=183;
	//
	new ScaleElementToWindowSize($(ID_MAIN_IMAGE), $(ID_MAIN_IMAGE_CONTAINER), MAIN_IMAGE_WIDTH, MAIN_IMAGE_HEIGHT);

	//
	//new ResizeElementToWindowSize($("#js-nav-inner"), true, true, verticalCenterNavLinks);




	//
	var ID_NAV_MENU="#js-menu";
	//var togglerNavMenu=new Toggler($(ID_NAV_MENU), showNav, hideNav);
	var navOverlayInterval=null;
	var togglerNavMenu=new Toggler($(ID_NAV_MENU), function(){
		showNav();

		//trick to update elements relaying on 100%height on smartphones with annoying topbars
		//adding space on .after seems to update the height
		var currentWindowHeight=$(window).height();
		navOverlayInterval=setInterval(function(){
			$("#js-nav-link-0").before(" ");
			$("#js-nav-link-0").after(" ");
			if( $(window).height() > currentWindowHeight ){
				clearInterval(navOverlayInterval);
			}
		}, 1);

	}, function(){
		hideNav();
		clearInterval(navOverlayInterval);
	});
	//
	$("html, body").on("touchmove", function(e){
		if(togglerNavMenu.isOn) e.preventDefault();
	});



	//
	var ID_FEATURE_CONTENT="#js-feature-content";
	var ID_FEATURE_PAGER_PREV="#js-feature-pager-prev";
	var ID_FEATURE_PAGER_NEXT="#js-feature-pager-next";
	var CLASS_NAME_IS_DISABLED="feature__pager--is-disabled";
	//
	$(ID_FEATURE_CONTENT).on("beforeChange", function(event, slick, currentSlide, nextSlide){
		//
		$(ID_FEATURE_PAGER_PREV).removeClass(CLASS_NAME_IS_DISABLED);
		$(ID_FEATURE_PAGER_NEXT).removeClass(CLASS_NAME_IS_DISABLED);
	 	//
	 	if(nextSlide == 2){
			$(ID_FEATURE_PAGER_NEXT).addClass(CLASS_NAME_IS_DISABLED);
		}else if(nextSlide == 0){
			$(ID_FEATURE_PAGER_PREV).addClass(CLASS_NAME_IS_DISABLED);
		}
	});
	$(ID_FEATURE_CONTENT).slick({
        autoplay: false,
        arrows: true,
        prevArrow: $(ID_FEATURE_PAGER_PREV),
        nextArrow: $(ID_FEATURE_PAGER_NEXT),
        infinite: false
    });
    


    //scrollMagic controller
	//var controller=new ScrollMagic.Controller();

	//
	//sectionCatchAnimation(controller);
	//sectionFeatureAnimation(controller);






	//new SequenceScroller();
	//test
	/*
	$("#js-nav-menu").click(function(){
		$("#js-nav-inner").removeClass("is-hidden");
		$("#js-nav-menu-graphic").addClass("is-active");
		//vertical center nav links
		let numHeightDifference=$("#js-nav-inner").height() - $("#js-nav-list").height();
		$("#js-nav-list").css("margin-top", numHeightDifference/2);
	});*/

	//smoothScrollTo( $("#js-section-catch") );

	/*
	$("#js-nav-down").click(function(){
		smoothScrollTo( $("#js-section-catch") );
	});*/

	/*
	var ID_NAV_DOWN="#js-nav-down";
	var ID_SECTION_TOP="#js-section-top";
	var ID_SECTION_CATCH="#js-section-catch";
	var ID_SECTION_FEATURE="#js-section-feature";
	new SectionSequenceScroller($(ID_NAV_DOWN), [
		$(ID_SECTION_TOP),
		$(ID_SECTION_CATCH),
		$(ID_SECTION_FEATURE)
	]);*/

	/*
	//scrollMagic controller
	var controller=new ScrollMagic.Controller();

	//
	sectionCatchAnimation(controller);

	//
	sectionFeatureAnimation(controller);

	//scroll spy
	var numPrevHeight=$(window).height();
	var isHeightChanged=false;
	var arrScrollSpyScenes=null;
	$(window).resize(function(){
		if( numPrevHeight != $(window).height() ){
			numPrevHeight=$(window).height();
			isHeightChanged=true;
		}
	});
	$(window).scroll(function(){
		if(isHeightChanged == false) return;
		//destroy prev scenes
		for(let i=0; i < arrScrollSpyScenes.length; i++){
			arrScrollSpyScenes[i].remove();
			arrScrollSpyScenes[i].destroy(true);
		}
		//
		isHeightChanged=false;
		arrScrollSpyScenes=navScrollSpyAnimation(controller);
	});
	arrScrollSpyScenes=navScrollSpyAnimation(controller);*/

});




//functions ------------------------------------------------------------------------------------------------------------------------
function navScrollSpyAnimation(scrollMagicController){
	var ID_HEADER="#js-header";
	var ID_SECTION_TOP="#js-section-top";
	var ID_SECTION_FEATURE="#js-section-feature";
	var ID_SECTION_SERVICE="#js-section-service";
	var ID_SECTION_NEWS="#js-section-news";
	var ID_NAV_ITEM_TOP="#js-nav-item-top";
	var ID_NAV_ITEM_FEATURE="#js-nav-item-feature";
	var ID_NAV_ITEM_SERVICE="#js-nav-item-service";
	var ID_NAV_ITEM_NEWS="#js-nav-item-news";
	var CLASS_NAME_IS_SELECTED="is-selected";
	//
	var numHeaderHeight=$(ID_HEADER)[0].getBoundingClientRect().height;
	var numAdditionalOffset=70 * 2; //based from section bottom padding
	var numViewPort=$(window).height() - numHeaderHeight - numAdditionalOffset;
	//
	var scene1=new ScrollMagic.Scene({
		triggerElement:$(ID_SECTION_TOP)[0],
		triggerHook:"onEnter",
		duration:$(ID_SECTION_TOP)[0].getBoundingClientRect().height,
		offset:numViewPort
	})
		.setClassToggle($(ID_NAV_ITEM_TOP)[0], CLASS_NAME_IS_SELECTED)
		.addTo(scrollMagicController);
	//
	var scene2=new ScrollMagic.Scene({
		triggerElement:$(ID_SECTION_FEATURE)[0],
		triggerHook:"onEnter",
		duration:$(ID_SECTION_FEATURE)[0].getBoundingClientRect().height,
		offset:numViewPort
	})
		.setClassToggle($(ID_NAV_ITEM_FEATURE)[0], CLASS_NAME_IS_SELECTED)
		.addTo(scrollMagicController);
	//
	var scene3=new ScrollMagic.Scene({
		triggerElement:$(ID_SECTION_SERVICE)[0],
		triggerHook:"onEnter",
		duration:$(ID_SECTION_SERVICE)[0].getBoundingClientRect().height,
		offset:numViewPort
	})
		.setClassToggle($(ID_NAV_ITEM_SERVICE)[0], CLASS_NAME_IS_SELECTED)
		.addTo(scrollMagicController);
	//
	var scene4=new ScrollMagic.Scene({
		triggerElement:$(ID_SECTION_NEWS)[0],
		triggerHook:"onEnter",
		duration:$(ID_SECTION_NEWS)[0].getBoundingClientRect().height,
		offset:numViewPort
	})
		.setClassToggle($(ID_NAV_ITEM_NEWS)[0], CLASS_NAME_IS_SELECTED)
		.addTo(scrollMagicController);
	//
	return [scene1, scene2, scene3, scene4];
}

function sectionCatchAnimation(scrollMagicController){
	//
	var ID_SECTION_CATCH="#js-section-catch";
	var ID_SECTION_CATCH_TITLE="#js-section-catch-title";
	var ID_SECTION_CATCH_DESCRIPTION="#js-section-catch-description";
	var ID_SECTION_CATCH_PHONE_1="#js-section-catch-phone-1";
	var ID_SECTION_CATCH_PHONE_2="#js-section-catch-phone-2";
	var ID_SECTION_CATCH_PHONE_3="#js-section-catch-phone-3";

	//
	var sceneCatch=new ScrollMagic.Scene({
		triggerElement:$(ID_SECTION_CATCH)[0],
		triggerHook:"onEnter",
		duration:$(window).height() + $(ID_SECTION_CATCH)[0].getBoundingClientRect().height,
		//duration:$(window).height(),
		offset:0
	});

	//
	scrollMagicController.addScene(sceneCatch);


	//simple effect class definition
	var ParallaxEffect=function ParallaxMovement(htmlElement, arrObjParams){
		this.htmlElement=htmlElement;
		this.arrObjParams=arrObjParams;
		//
		var objParam;
		for(var i=0; i < arrObjParams.length; i++){
			objParam=arrObjParams[i];
			objParam.result=0;
		}
	}
	ParallaxEffect.prototype.update=function(numProgress){
		var objParam;
		for(var i=0; i < this.arrObjParams.length; i++){
			objParam=this.arrObjParams[i];
			objParam.result=objParam.numStart + (objParam.numEnd - objParam.numStart) * (numProgress + objParam.numProgressOffset);
			$(this.htmlElement).css(objParam.strProp, objParam.result);	
		}
	}
	//use effect class
	var parallaxEffectCatchPhone1=new ParallaxEffect(
		$(ID_SECTION_CATCH_PHONE_1),
		[{
			strProp:"top",
			numStart:45 + 400,
			numEnd:45,
			numProgressOffset:0.5}]
	);
	var parallaxEffectCatchPhone2=new ParallaxEffect(
		$(ID_SECTION_CATCH_PHONE_2),
		[{
			strProp:"top",
			numStart:95 + 200,
			numEnd:95,
			numProgressOffset:0.5}]
	);
	var parallaxEffectCatchPhone3=new ParallaxEffect(
		$(ID_SECTION_CATCH_PHONE_3),
		[{
			strProp:"top",
			numStart:-48 + 100,
			numEnd:-48,
			numProgressOffset:0.5}]
	);
	/*
	var parallaxEffectCatchTitle=new ParallaxEffect(
		$(ID_SECTION_CATCH_TITLE),
		[{
			strProp:"top",
			numStart:225 + 100,
			numEnd:225,
			numProgressOffset:0.5}]
	);
	var parallaxEffectCatchDescription=new ParallaxEffect(
		$(ID_SECTION_CATCH_DESCRIPTION),
		[{
			strProp:"top",
			numStart:400 + 100,
			numEnd:400,
			numProgressOffset:0.5}]
	);*/
	//update on event
	sceneCatch.on("progress", function(e){
		//console.log(e.progress);
		parallaxEffectCatchPhone1.update(e.progress);
		parallaxEffectCatchPhone2.update(e.progress);
		parallaxEffectCatchPhone3.update(e.progress);
		//parallaxEffectCatchTitle.update(e.progress);
		//parallaxEffectCatchDescription.update(e.progress);
	});
}

function sectionFeatureAnimation(scrollMagicController){
	//
	var ID_PREFIX="#js-feature-";
	var CLASS_NAME_TOGGLE="--is-shown";
	var arrStrArticles=["-html5", "-css3", "-javascript"];
	//
	for(let i=0; i < arrStrArticles.length; i++){

		let strArticle=arrStrArticles[i];

		new ScrollMagic.Scene({
			triggerElement:$(ID_PREFIX + "image" + strArticle)[0],
			triggerHook:"onEnter",
		})	
			.setClassToggle( $(ID_PREFIX + "image" + strArticle)[0], "feature__article-image" + CLASS_NAME_TOGGLE )
			.addTo(scrollMagicController);

		new ScrollMagic.Scene({
			triggerElement:$(ID_PREFIX + "title" + strArticle)[0],
			triggerHook:1,
		})	
			.setClassToggle( $(ID_PREFIX + "title" + strArticle)[0], "feature__article-title" + CLASS_NAME_TOGGLE )
			.addTo(scrollMagicController);

		new ScrollMagic.Scene({
			triggerElement:$(ID_PREFIX + "description" + strArticle)[0],
			triggerHook:1,
		})	
			.setClassToggle( $(ID_PREFIX + "description" + strArticle)[0], "feature__article-description" + CLASS_NAME_TOGGLE )
			.addTo(scrollMagicController);
		//console.log($(ID_PREFIX + "image" + strArticle)[0]);
	}
}

function showNav(){
	//
	var ID_NAV="#js-nav";
	var CLASS_NAV_HIDDEN="nav--is-hidden";
	$(ID_NAV).removeClass(CLASS_NAV_HIDDEN);

	//
	var ID_MENU_GRAPHIC="#js-menu-graphic";
	var CLASS_MENU_GRAPHIC_HAMBURGER="menu__graphic-hamburger";
	var CLASS_MENU_GRAPHIC_CLOSE="menu__graphic-close";
	$(ID_MENU_GRAPHIC).removeClass(CLASS_MENU_GRAPHIC_HAMBURGER);
	$(ID_MENU_GRAPHIC).addClass(CLASS_MENU_GRAPHIC_CLOSE);

	//
	//verticalCenterNavLinks();
}

function hideNav(){
	//
	var ID_NAV="#js-nav";
	var CLASS_NAV_HIDDEN="nav--is-hidden";
	$(ID_NAV).addClass(CLASS_NAV_HIDDEN);

	//
	var ID_MENU_GRAPHIC="#js-menu-graphic";
	var CLASS_MENU_GRAPHIC_HAMBURGER="menu__graphic-hamburger";
	var CLASS_MENU_GRAPHIC_CLOSE="menu__graphic-close";
	$(ID_MENU_GRAPHIC).addClass(CLASS_MENU_GRAPHIC_HAMBURGER);
	$(ID_MENU_GRAPHIC).removeClass(CLASS_MENU_GRAPHIC_CLOSE);
}

function verticalCenterNavLinks(){
	var ID_NAV_INNER="#js-nav-inner";
	var ID_NAV_LIST="#js-nav-list";
	//
	//let numHeightDifference=$(ID_NAV_INNER).height() - $(ID_NAV_LIST).height();
	let numHeightDifference=$(window).height() - $(ID_NAV_INNER).height();
	$(ID_NAV_INNER).css("margin-top", numHeightDifference/2);
	//console.log($(window).height());

}

/*
function smoothScrollTo(htmlElementTarget){
	//inner functions copied from module/smooth-scroll.js
	function mStopOn(){
		$(document).on('DOMMouseScroll', preventDefault);
	    $(document).on('mousewheel', preventDefault);
	}

	function mStopOff(){
		$(document).off('DOMMouseScroll', preventDefault);
	    $(document).off('mousewheel', preventDefault);
	}

	function preventDefault(event){
		event.preventDefault();
	}
	//
	mStopOn();
	$('html, body').animate({scrollTop:$(htmlElementTarget)[0].getBoundingClientRect().top}, 500, mStopOff);
}
*/
//classes ------------------------------------------------------------------------------------------------------------------------
var ScaleElementToWindowSize=(function(){
	function ScaleElementToWindowSize(htmlElementTarget, htmlElementTargetContainer, numMinWidth, numMinHeight){
		this.htmlElementTarget=htmlElementTarget;
		this.htmlElementTargetContainer=htmlElementTargetContainer;
		this.numMinWidth=numMinWidth;
		this.numMinHeight=numMinHeight;

		//
		this.initEvents();

		//
		this.update();
	}

	ScaleElementToWindowSize.prototype.initEvents=function(){
		var _this=this;
		$(window).resize(function(){
			_this.update();
		});
	}

	ScaleElementToWindowSize.prototype.update=function(){
		var numScale=1;

		//initially fit 100% to the lowest side
		if( $(window).height() < $(window).width() ){
			//console.log("h");
			numScale=$(window).height() / this.numMinHeight;
		}else{
			//console.log("w");
			numScale=$(window).width() / this.numMinWidth;
		}
		//numScale=numScale - 1;
		//console.log($(window).height(), numMinHeight, numScale);
		$(this.htmlElementTarget).css("min-width", this.numMinWidth);
		$(this.htmlElementTarget).css("min-height", this.numMinHeight);
		$(this.htmlElementTarget).css("transform-origin", "left top");
		$(this.htmlElementTarget).css("transform", "scale("+ numScale +")");
		
		//
		var widthDifference=$(window).width() - this.htmlElementTarget[0].getBoundingClientRect().width;
		var heightDifference=$(window).height() - this.htmlElementTarget[0].getBoundingClientRect().height;

		//
		//console.log("---");
		//console.log( $(window).width(), $(window).height() );
		//console.log( widthDifference, heightDifference );
		//console.log( numScale );

		
		//if there is extra in differences, refit the side with difference
		if(widthDifference > 0){
			numScale=$(window).width() / this.numMinWidth;
		}
		if(heightDifference > 0){
			numScale=$(window).height() / this.numMinHeight;
		}
		$(this.htmlElementTarget).css("transform-origin", "left top");
		$(this.htmlElementTarget).css("transform", "scale("+ numScale +")");

		//update the container box dimensions
		$(this.htmlElementTargetContainer).css("width", this.htmlElementTarget[0].getBoundingClientRect().width);
		$(this.htmlElementTargetContainer).css("height", this.htmlElementTarget[0].getBoundingClientRect().height);

		//console.log(this.htmlElementTarget[0].getBoundingClientRect().width, this.htmlElementTarget[0].getBoundingClientRect().height);
	}

	return ScaleElementToWindowSize;
}());

var ResizeElementToWindowSize=(function(){
	function ResizeElementToWindowSize(htmlElementTarget, doResizeWidth=true, doResizeHeight=true, onResizeCallBack=null){
		//todo: implement resize width;

		//
		this.htmlElementTarget=htmlElementTarget;
		this.doResizeWidth=doResizeWidth;
		this.doResizeHeight=doResizeHeight;
		this.onResizeCallBack=onResizeCallBack;

		//
		this.initEvents();

		//
		this.update();
	}

	ResizeElementToWindowSize.prototype.initEvents=function(){
		var _this=this;
		$(window).resize(function(){
			_this.update();
		});
	}

	ResizeElementToWindowSize.prototype.update=function(){
		if(this.doResizeHeight){
			//$(this.htmlElementTarget).css("height", $(window).height());
			//alert( $(window).height() + "-" + $(window).outerHeight() );

			//$(this.htmlElementTarget).css("height", $(window).outerHeight());
			//$(this.htmlElementTarget).css("height", "100%");

			this.onResizeCallBack();
		}
	}

	return ResizeElementToWindowSize;
}());

var StickyHeader=(function(){
	function StickyHeader(htmlElementTarget, strClassNameSticky, strCloneSuffix="-clone"){
		//
		this.htmlElementTarget=htmlElementTarget;
		this.strClassNameSticky=strClassNameSticky;
		this.strCloneSuffix=strCloneSuffix;

		//
		this.initEvents();

		//
		this.update();
	}

	StickyHeader.prototype.initEvents=function(){
		//
		var _this=this;

		//
		$(window).scroll(function(){
			_this.update();
		});
	}

	StickyHeader.prototype.update=function(){
		//
		var strCloneID="#" + $(this.htmlElementTarget).attr("id") + this.strCloneSuffix;

		//
		var htmlElementTargetClone=null;
		if( $(strCloneID).length == 0){
			htmlElementTargetClone=$("<div>&nbsp;</div>");
			htmlElementTargetClone.attr("id", $(this.htmlElementTarget).attr("id") + this.strCloneSuffix);
			htmlElementTargetClone.attr("class", $(this.htmlElementTarget).attr("class") );
			htmlElementTargetClone.css("position", "absolute");
			//htmlElementTargetClone.css("background-color", "#ff0000");

			//
			$(this.htmlElementTarget).before( htmlElementTargetClone );
		}else{
			htmlElementTargetClone=$(strCloneID);
		}

		//
		if( $(htmlElementTargetClone)[0].getBoundingClientRect().top <= 0 ){
			htmlElementTargetClone.css("position", "static");
			$(this.htmlElementTarget).addClass(this.strClassNameSticky);
		}else{
			htmlElementTargetClone.css("position", "absolute");
			$(this.htmlElementTarget).removeClass(this.strClassNameSticky);
		}
	}

	return StickyHeader;
}());

var Toggler=(function(){
	function Toggler(htmlElementToggleTarget, toggleOnCallBack, toggleOffCallBack){
		//
		this.htmlElementToggleTarget=htmlElementToggleTarget;
		this.toggleOnCallBack=toggleOnCallBack;
		this.toggleOffCallBack=toggleOffCallBack;

		//
		this.isOn=false;

		//
		this.initEvents();
	}

	Toggler.prototype.initEvents=function(){
		var _this=this;
		$(this.htmlElementToggleTarget).click(function(){
			//alert($(window).outerHeight(), $(window).height(), $(document).outerHeight(), $(document).height());
			if(_this.isOn){
				_this.isOn=false;
				_this.toggleOffCallBack();
			}else{
				_this.isOn=true;
				_this.toggleOnCallBack();
			}
		});
	}

	return Toggler;
}());
/*
var SectionSequenceScroller=(function(){
	function SectionSequenceScroller(htmlElementTrigger, arrHtmlElementSections){
		this.htmlElementTrigger=htmlElementTrigger;
		this.arrHtmlElementSections=arrHtmlElementSections;

		//
		this.numCurrentSectionIndex=0;

		//get current scroll position
		console.log("---");
		console.log( $(window).scrollTop() );
		for(let i=0; i < arrHtmlElementSections.length; i++){
			//console.log(i, arrHtmlElementSections[i]);
			console.log( $(arrHtmlElementSections[i])[0].getBoundingClientRect().top );
		}


		//
		this.initEvents();
	}

	SectionSequenceScroller.prototype.initEvents=function(){
		$(this.htmlElementTrigger).click(function(){

		});
	}

	SectionSequenceScroller.prototype.scrollToNext=function(){

	}

	return SectionSequenceScroller;
}());*/
