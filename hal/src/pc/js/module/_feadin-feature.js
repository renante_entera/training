export default class FeadinFeature {
    constructor() {
        this.$feature = document.getElementById('js-feature');
        this.controller = new ScrollMagic.Controller();
        this.bind();
    }

    bind() {
        new ScrollMagic.Scene({
            triggerElement: this.$feature,
            triggerHook: 0.3
        })
            .setClassToggle(this.$feature, 'is-show')
            .addTo(this.controller);
    }


}