import $ from 'jquery';
export default class ScrollLink {
    constructor() {
        this.adjustmentHeight = 70; //セクションの上マージン
        this.action();
    }

    action() {
        $('a[href^="#"]').on('click',(target)=>{this.handleClick(target)});
    }

    handleClick(self) {
        let href= $(self.currentTarget).attr('href');

        let position = 0;
        if (href !=='#top'){
            let target = $(href == '#' || href == '' ? 'html' : href);
            position = target.offset().top;
            position -= this.adjustmentHeight;
        }

        $('html,body').animate({scrollTop: position}, 500, 'swing');
        return false;
    }



}