export default class ParallaxPhone {
    constructor() {
        this.controller = new ScrollMagic.Controller();
        this.$catch = document.getElementById('js-catch');
        this.targetData = [
            ['js-phone1',500,250],
            ['js-phone2',1000,500],
            ['js-phone3',1000,750]
        ];
        this.parallax(this.targetData);
    }

    parallax (targetData) {
        for ( let target of targetData) {
            let $phone = document.getElementById(target[0]);
            new ScrollMagic.Scene({
                triggerElement: this.$catch,
                triggerHook: 'onCenter',
                duration: target[1]
            })
                .on('progress', function (prog) {
                    let progress = prog.progress;
                    $phone.style.transform = 'translate3d(0,' + -(progress * target[2]) + 'px,0)';
                })
                .addTo(this.controller);

        }
    }


}