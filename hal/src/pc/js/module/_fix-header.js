// import $ from 'jquery';
import ScrollMagic from 'scrollmagic';
export default class FixHeader {
    constructor() {
        this.controller = new ScrollMagic.Controller();
        this.scene;
        this.$target = document.getElementById('js-header');
        this.$trigger = document.getElementById('js-catch');
        this.bind();
    }
    
    bind() {
        this.scroll();
    }

    scroll() {
        this.scene = new ScrollMagic.Scene({
            triggerElement: this.$trigger,
            triggerHook: 0.1
        })
            .setPin(this.$target)
            .addTo(this.controller);
        ;
    }
}