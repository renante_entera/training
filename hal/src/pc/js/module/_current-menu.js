import $ from 'jquery';
export default class CurrentMenu {
    constructor(targetId,targetHeaderId) {
        this.$target = $(targetId);
        this.$headerTarget = $(targetHeaderId);
        this.adjustmentHeight = 70;
        this.bind();
    }
    bind() {
        this.currentAction();
    }

    currentAction() {
        window.addEventListener('scroll', ()=>{this.handleScroll()});
    }

    handleScroll() {
        let contentsTop = this.$target.offset().top - this.adjustmentHeight;
        let contentsArea = contentsTop + this.$target.innerHeight();

        let windowTop = $(window).scrollTop();
        if (contentsTop <= windowTop && windowTop < contentsArea) {
            // 処理
            this.$headerTarget.addClass('is-current');
        } else {
            this.$headerTarget.removeClass('is-current');
        }
    }

}