import $ from 'jquery';
import FixHeader from './module/_fix-header';
import FeadinFeature from './module/_feadin-feature';
import ParallaxPhone from './module/_parallax-phone';
import CurrentMenu from './module/_current-menu';
import ScrollLink from './module/_scrollLink';

window.addEventListener('load', function() {
    new FixHeader();

    new FeadinFeature();
    new ParallaxPhone();
    new CurrentMenu('#js-content-top','#js-header-nav-list-top');
    new CurrentMenu('#js-content-feature','#js-header-nav-list-faeture');
    new CurrentMenu('#js-content-service','#js-header-nav-list-service');
    new CurrentMenu('#js-content-news','#js-header-nav-list-news');
    // [
    //     'js-content-feature',
    //     'js-content-service',
    //     'js-content-news'
    // ]
    new ScrollLink();


});